# Introduction to GitOps

Presentation Slides for:
- talk at [Denver Dev Day](https://denverdevday.github.io/jun-2023/) - Jun 9, 2023

Resources:
- [The Presentation Slides](https://dave54.gitlab.io/introduction-to-gitops/)
- [ArgoCD](https://argoproj.github.io/cd/)
- [Cluster API](https://cluster-api.sigs.k8s.io/)
- [Azure Service Operator](https://github.com/Azure/azure-service-operator)
- [Crossplane](https://www.crossplane.io/)
- [Nills Franssens's Blog](https://blog.nillsf.com/)
- [Nills Franssens on GitHub](https://github.com/NillsF)
